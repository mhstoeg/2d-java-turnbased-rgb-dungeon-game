package engine.actions;

import engine.Camera;
import engine.Renderer;
import engine.Updatable;
import engine.input.InputManager;

public class ActionManager implements Updatable {

    private ActionQueue actionQueue;
    private InputManager inputManager;

    private ActionLoader actionLoader;

    public ActionManager() {
        setActionQueue(new ActionQueue());
        setInputManager(new InputManager());
        setActionLoader(new ActionLoader());
    }

    public void update(Renderer renderer, Camera camera) {
        getInputManager().update();
        determineActions();
        processActions(renderer, camera);
    }

    public void determineActions() {
        for (Action action : actionLoader.getActions()) {
            if (getInputManager().isActivated(action.getInput(), action.getInputType())) {
                getActionQueue().addAction(action.getActionType());
            }
        }
    }

    public void processActions(Renderer renderer, Camera camera) {

        while (getActionQueue().unprocessedActions()) {
            ActionType actionType = getActionQueue().processAction();

            switch (actionType) {
                case PAN_CAMERA:
                    panCamera(camera);
                    break;
                case ZOOM_IN:
                    zoomIn(renderer, camera);
                    break;
                case ZOOM_OUT:
                    zoomOut(renderer, camera);
                    break;
                case MOVE_CAMERA_UP:
                    camera.moveCamera(0, -camera.getSpeed());
                    break;
                case MOVE_CAMERA_DOWN:
                    camera.moveCamera(0, camera.getSpeed());
                    break;
                case MOVE_CAMERA_LEFT:
                    camera.moveCamera(-camera.getSpeed(), 0);
                    break;
                case MOVE_CAMERA_RIGHT:
                    camera.moveCamera(camera.getSpeed(), 0);
                    break;
            }
        }
    }

    public void panCamera(Camera camera) {
        int x = getInputManager().getDragAmountX();
        int y = getInputManager().getDragAmountY();
        camera.moveCamera(-x, -y);
    }

    public void zoomIn(Renderer renderer, Camera camera) {
        double growthFactor = 0.5;
        // growthFactor must be >0 and <= 2, ideally a multiple of 0.5
        if (renderer.getScale() < 8) {
            renderer.setScale(renderer.getScale() + growthFactor);
            renderer.setScaleChanged(true);
            camera.moveCamera(-16*12/(int)(2/growthFactor), -16*8/(int)(2/growthFactor));

            // move camera by squareLength * lengthOfTileMap/4,  squareLength * lengthOfTileMap/4
            // we need to divide by 4 to give the illusion that as we zoom in, the tilemap grows
            // equally in all directions. since increasing an images size grows it down and right on
            // the screen, we need to move the camera in the opposite direction by by halve the amount it grows.
            // in this case, we are growing the tilemap so we need to move it in the opposite directing by half,
            //
        }


        //TODO
        // for scaling to work correctily, we should update the square and tile lengths to match the scale
        // and the camera worldX and worldY should be moved to the same position relative to the tilemap

    }
    public void zoomOut(Renderer renderer, Camera camera) {
        double growthFactor = 0.5;
        if (renderer.getScale() > 1) {
            renderer.setScale(renderer.getScale() - growthFactor);
            renderer.setScaleChanged(true);
            camera.moveCamera(16*12/(int)(2/growthFactor), 16*8/(int)(2/growthFactor));
        }

        //TODO
        // for scaling to work correctily, we should update the square and tile lengths to match the scale
        // and the camera worldX and worldY should be moved to the same position relative to the tilemap

    }

    public ActionLoader getActionLoader() {
        return actionLoader;
    }

    public void setActionLoader(ActionLoader actionLoader) {
        this.actionLoader = actionLoader;
    }

    public ActionQueue getActionQueue() {
        return actionQueue;
    }

    public void setActionQueue(ActionQueue actionQueue) {
        this.actionQueue = actionQueue;

    }

    public InputManager getInputManager() {
        return inputManager;
    }

    public void setInputManager(InputManager inputManager) {
        this.inputManager = inputManager;
    }
}
