package engine.util.data_structure;

public class Node {

    private Object key;
    private Node next;

    public Node(Object key) {
        setKey(key);
        setNext(null);
    }

    public Object getKey() {
        return key;
    }

    public void setKey(Object key) {
        this.key = key;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }
}
