package engine.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public abstract class FileLineCounter {

    public static int count(String filePath) {
        int count = 0;
        try {
            InputStream is = FileLineCounter.class.getResourceAsStream(filePath);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String line = br.readLine();
            while (line != null) {
                count++;
                line = br.readLine();
            }
            br.close();
        } catch (Exception e) {

        }
        return count;
    }
}