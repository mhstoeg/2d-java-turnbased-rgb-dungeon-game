package engine.util.image;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public abstract class ImageLoader {
    public static BufferedImage loadImage(String filePath, String imageName, int length) {

        BufferedImage image = null;
        try {
            image = ImageIO.read(ImageLoader.class.getResourceAsStream(filePath + imageName + ".png"));
            image = ImageScaler.scaleImage(image, length, length);

        } catch(IOException e) {
            e.printStackTrace();
            System.out.println("Could not load in Image");
        }
        return image;
    }
}
