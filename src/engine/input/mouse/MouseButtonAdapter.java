package engine.input.mouse;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MouseButtonAdapter implements MouseListener {

    private boolean lmbPressed, rmbPressed, scrollPressed, lmbClicked, rmbClicked, scrollClicked;


    public MouseButtonAdapter() {
        setLmbPressed(false);
        setRmbPressed(false);
        setScrollPressed(false);
        setLmbClicked(false);
        setRmbClicked(false);
        setScrollClicked(false);

    }

    public boolean isLmbClicked() {
        boolean previousState = lmbClicked;
        setLmbClicked(false);
        return previousState;
    }

    private void setLmbClicked(boolean lmbClicked) {
        this.lmbClicked = lmbClicked;
    }

    public boolean isRmbClicked() {
        boolean previousState = rmbClicked;
        setRmbClicked(false);
        return previousState;
    }

    private void setRmbClicked(boolean rmbClicked) {
        this.rmbClicked = rmbClicked;
    }

    public boolean isScrollClicked() {
        boolean previousState = scrollClicked;
        setScrollClicked(false);
        return previousState;
    }

    private void setScrollClicked(boolean scrollClicked) {
        this.scrollClicked = scrollClicked;
    }

    public boolean isLmbPressed() {
        return lmbPressed;
    }

    private void setLmbPressed(boolean lmbPressed) {
        this.lmbPressed = lmbPressed;
    }

    public boolean isRmbPressed() {
        return rmbPressed;
    }

    private void setRmbPressed(boolean rmbPressed) {
        this.rmbPressed = rmbPressed;
    }

    public boolean isScrollPressed() {
        return scrollPressed;
    }

    private void setScrollPressed(boolean scrollPressed) {
        this.scrollPressed = scrollPressed;
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

        int button = mouseEvent.getButton();

        if (button == MouseButton.LMB.getValue()) {
            setLmbClicked(true);
        }

        if (button == MouseButton.SCROLL.getValue()) {
            setScrollClicked(true);
        }

        if (button == MouseButton.RMB.getValue()) {
            setRmbClicked(true);
        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

        int button = mouseEvent.getButton();

        if (button == MouseButton.LMB.getValue()) {
            setLmbPressed(true);
        }

        if (button == MouseButton.SCROLL.getValue()) {
            setScrollPressed(true);
        }

        if (button == MouseButton.RMB.getValue()) {
            setRmbPressed(true);
        }

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

        int button = mouseEvent.getButton();

        if (button == MouseButton.LMB.getValue()) {
            setLmbPressed(false);
        }

        if (button == MouseButton.SCROLL.getValue()) {
            setScrollPressed(false);
        }

        if (button == MouseButton.RMB.getValue()) {
            setRmbPressed(false);
        }
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {
    }
}
