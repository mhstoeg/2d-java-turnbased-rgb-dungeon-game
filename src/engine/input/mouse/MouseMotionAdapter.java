package engine.input.mouse;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class MouseMotionAdapter implements MouseMotionListener {

    private boolean moved, dragged;
    private int x, y;


    public MouseMotionAdapter() {
        setMoved(false);
        setDragged(false);
        setX(0);
        setY(0);
    }

    public int getX() {
        return x;
    }

    private void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    private void setY(int y) {
        this.y = y;
    }

    public boolean isDragged() {
        boolean previousState = dragged;
        setDragged(false);
        return previousState;
    }

    private void setDragged(boolean dragged) {
        this.dragged = dragged;
    }

    public boolean isMoved() {
        boolean previousState = moved;
        setMoved(false);
        return previousState;
    }

    private void setMoved(boolean moved) {
        this.moved = moved;
    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        setDragged(true);
        setX(mouseEvent.getX());
        setY(mouseEvent.getY());
    }


    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
        setMoved(true);
        setX(mouseEvent.getX());
        setY(mouseEvent.getY());
    }
}
