package engine.sound;

public interface MusicPlayer {


    void playMusic(int i);

    void stopMusic();

    void playEffect(int i);
}
